///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Ryan Edquiba <redquiba@hawaii.edu>
/// @date 17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

const double CAT_POWER_IN_A_JOULE      = 0;
const char CAT_POWER     = 'c';

extern double fromCatPowerToJoule( double catPower ) ;
extern double fromJouleToCatPower( double joule ) ;
