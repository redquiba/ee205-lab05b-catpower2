/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Ryan Edquiba <redquiba@hawaii.edu>
/// @date 17_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#include "gge.h"
double fromGasGallonToJoule( double gasGallon ) {
   return gasGallon / GAS_GALLON_IN_A_JOULE;
}
double fromJouleToGasGallon( double joule ) {
   return joule * GAS_GALLON_IN_A_JOULE;
}

