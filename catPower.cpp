///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Ryan Edquiba <redquiba@hawaii.edu>
/// @date   2/17/22
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "ev.h"
#include "megaton.h"
#include "gge.h"
#include "foe.h"
#include "cat.h"

// A Joule represents the amount of electricity required to run a 1 W device for 1 s
const char JOULE         = 'j';
#define DEBUG
int main( int argc, char* argv[] ) {

   double fromValue;
   char   fromUnit;
   char   toUnit;

 if(argc > 4 )
 {
   printf( "Usage incorrect, too many arguments.\nPlease enter: [Number] [Enery unit of number] [Enery unit to convert to]\n" );
 }
 else if(argc < 4 )
 {
   printf( "Usage incorrect, too few arguments.\nPlease enter: [Number] [Enery unit of number] [Enery unit to convert to]\n" );
 }
 else
 {
   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument

#ifdef DEBUG
   printf( "fromValue = [%lG]\n", fromValue );   
   printf( "fromUnit = [%c]\n", fromUnit );      
   printf( "toUnit = [%c]\n", toUnit );          
#endif

   double commonValue;
   switch( fromUnit ) {
      case JOULE         : commonValue = fromValue; // No conversion necessary
                           break;
      case ELECTRON_VOLT : commonValue = fromElectronVoltsToJoule( fromValue );
                           break;
      case MEGATON       : commonValue = fromMegatonToJoule( fromValue);
                           break;
      case GAS_GALLON    : commonValue = fromGasGallonToJoule( fromValue);
                           break;
      case FOE           : commonValue = fromFoeToJoule( fromValue);
                           break;
      case CAT_POWER     : commonValue =  fromCatPowerToJoule( fromValue );
                           break;
      default            :
         printf( "Unknown fromUnit [%c]\n", fromUnit );
         exit( EXIT_FAILURE );

   }
#ifdef DEBUG
   printf( "commonValue = [%lG] joule\n", commonValue ); 
#endif

   double toValue;
   switch( toUnit ) {
      case JOULE         : toValue = commonValue; // No Conversion necessary 
                           break;
      case ELECTRON_VOLT : toValue = fromJouleToElectronVolts( commonValue );
                           break;
      case MEGATON       : toValue = fromJouleToMegaton( commonValue );
                           break;
      case GAS_GALLON    : toValue = fromJouleToGasGallon( commonValue );
                           break;
      case FOE           : toValue = fromJouleToFoe( commonValue );
                           break;
      case CAT_POWER     : toValue = fromJouleToCatPower( commonValue );
                           break;
      default            :
         printf( "Unknown toUnit [%c]\n", toUnit );
         exit( EXIT_FAILURE );

   }
 
 printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
 }
}
