///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Ryan Edquiba <redquiba@hawaii.edu>
/// @date 17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

const double GAS_GALLON_IN_A_JOULE     = 1/1.213e8;
const char GAS_GALLON    = 'g';

extern double fromGasGallonToJoule( double gasGallon ) ;
extern double fromJouleToGasGallon( double joule ) ;
